# OPluginManager
# Copyright (C) 2005, 2006 Nicolas Trangez <eikke@eikke.com>.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
# Boston, MA 02110-1301, USA.

class OPluginManagerPluginAuthor:
        def __init__(self, name, email = "", uri = ""):
                self.name = name
                self.email = email
                self.uri = uri

        def GetName(self):
                return self.name

        def GetEmail(self):
                return self.email

        def GetUri(self):
                return self.uri

        def Dump(self, prefix = ""):
                print prefix + self.name
                print prefix + "--------"
                if self.email != "":
                        print prefix + "* Email: " + self.email
                else:
                        print prefix + "* Email not set"
                if self.uri != "":
                        print prefix + "* Uri: " + self.uri
                else:
                        print prefix + "* Uri not set"
                print ""
                

class OPluginManagerPlugin:
        def __init__(self, plugin_magic, name, summary, authors, description = "", version = "", uri = "", init_func = 0, data_free_func = 0, configure_func = 0):
                print "Creating new OPluginManagerPythonPlugin instance: " + name
                self.plugin_magic = plugin_magic
                self.name = name
                self.summary = summary
                self.authors = authors
                self.description = description
                self.version = version
                self.uri = uri
                self.init_funcp = init_func
                self.data_free_funcp = data_free_func
                self.configure_funcp = configure_func

        def GetPluginMagic(self):
                return self.plugin_magic

        def GetName(self):
                return self.name

        def GetSummary(self):
                return self.summary

        def GetAuthor(self):
                return self.authors

        def GetDescription(self):
                return self.description

        def GetVersion(self):
                return self.version

        def GetUri(self):
                return self.uri

        def Dump(self):
                print "Dumping data for plugin " + self.name
                print "-----------------------"
                print "* Name: " + self.name
                print "* Summary: " + self.summary
                print "* Authors: "
                self.authors.Dump("      ")
                if self.description != "":
                        print "* Description: " + self.description
                if self.version != "":
                        print "* Version: " + self.version
                if self.uri != "":
                        print "* Uri: " + self.uri
                if self.init_funcp != 0:
                        print "* Plugin got an init function"
                if self.data_free_funcp != 0:
                        print "* Plugin got a data_free function"
                if self.configure_funcp != 0:
                        print "* Plugin got a configure function"

        def configure_func(self):
                if self.configure_funcp != 0:
                        self.configure_funcp()

        def init_func(self, init_data):
                if self.init_funcp != 0:
                        return self.init_funcp(init_data)

        def data_free_func(self, data):
                if self.data_free_funcp != 0:
                        self.data_free_funcp(data)
