/* OPluginManager
 * Copyright (C) 2005, 2006 Nicolas Trangez <eikke@eikke.com>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef _O_PLUGIN_MANAGER_PLUGIN_H
#define _O_PLUGIN_MANAGER_PLUGIN_H

#include <glib.h>
#include <gmodule.h>

#include "o-plugin-manager-plugin-data.h"

#define O_PLUGIN_MANAGER_PLUGIN_GET_DATA_FUNC_NAME _o_plugin_manager_plugin_get_data_func

typedef OPluginManagerPluginData * (*OPluginManagerPluginGetDataFunc) (void);

# define O_PLUGIN_MANAGER_PLUGIN_REGISTER(plugininfo) \
        G_MODULE_EXPORT OPluginManagerPluginData * O_PLUGIN_MANAGER_PLUGIN_GET_DATA_FUNC_NAME (void) { \
                return &(plugininfo); \
        }

#endif
