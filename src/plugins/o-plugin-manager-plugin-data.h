/* OPluginManager
 * Copyright (C) 2005, 2006 Nicolas Trangez <eikke@eikke.com>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef _O_PLUGIN_MANAGER_PLUGIN_DATA_H
#define _O_PLUGIN_MANAGER_PLUGIN_DATA_H
#include <glib.h>

#define O_PLUGIN_MANAGER_PLUGIN_DATA_MAGIC 132435
#define O_PLUGIN_MANAGER_PLUGIN_DATA_AUTHOR_MAGIC 465768

typedef void (*OPluginManagerPluginDataFreeFunc) (gpointer data);
typedef gpointer (*OPluginManagerPluginInitFunc) (gpointer init_data, GError **error);
typedef void (*OPluginManagerPluginConfigureFunc) (void);

struct _OPluginManagerPluginAuthorData { 
        /* Magic for this structure, to check whether it's a valid OPluginManagerPluginAuthorData struct */
        guint magic;

        /* Author name */
        /* NOT NULL */
        gchar *name;
        /* Author email */
        gchar *email;
        /* Author homepage */
        gchar *uri;
};
typedef struct _OPluginManagerPluginAuthorData OPluginManagerPluginAuthorData;

struct _OPluginManagerPluginData {
        /* Magic for this structure, just to check whether the value returned by the init function is a valid OPluginManagerPluginData struct */
        guint magic;
        /* Magic for the plugin, so when multiple plugins (for multiple programs) are in one directory, only the correct ones will be loader */
        gchar *plugin_magic;

        /* Name of the plugin */
        /* NOT NULL */
        gchar *name;
        /* Summary description */
        /* NOT NULL */
        gchar *summary;
        /* Long description */
        gchar *description;
        /* Version string. Maybe this should be a struct consisting of 3 ints (x.y.z), so we can provide version dependency functionality */
        gchar *version;

        /* URI, homepage of the module */
        gchar *uri;

        /* Plugin init func, "typedef gpointer (*OPluginInitFunc) (gpointer init_data, GError **error);"
           Returns arbitrary data, which can be used by the module client app */
        OPluginManagerPluginInitFunc init_func;
        /* Free the data returned by init_func, "typedef void (*OPluginDataFreeFunc) (gpointer data);" */
        OPluginManagerPluginDataFreeFunc data_free_func;
        /* Called when the user clicks on a "configure" button in the UI, "typedef void (*OPluginManagerPluginConfgureFunc) (void));" */
        OPluginManagerPluginConfigureFunc configure_func;
        
        /* This is on the end for binary compatibility */
        OPluginManagerPluginAuthorData *authors;
};
typedef struct _OPluginManagerPluginData OPluginManagerPluginData;

#endif
