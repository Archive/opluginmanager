/* OPluginManager
 * Copyright (C) 2005, 2006 Nicolas Trangez <eikke@eikke.com>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include <gtk/gtk.h>

#include "plugins/o-plugin-manager-plugin.h"
#include "plugins/o-plugin-manager-plugin-data.h"

#include "test-loader.h"

#define TEST_PLUGIN_MSG "[test-plugin1] "

/* This will most likely return a vtable with "real" plugin functions the server app will use/call */
static gpointer plugin_init(gpointer d, GError **e) {
        g_debug("" TEST_PLUGIN_MSG "Init with data \"%s\"", (const gchar *) d);
        return (gpointer) g_strdup("Test plugin 1 data");
}

static void free_data(gpointer d) {
        g_debug("" TEST_PLUGIN_MSG "Freeing data \"%s\"", (gchar *) d);

        /* "d" is the data produced by plugin_init, it's the plugin author's task to know how to free it */
        g_free(d);
}

static void configure() {
        GtkWidget *window = NULL, *button = NULL;
        
        g_debug("" TEST_PLUGIN_MSG "Starting configuration dialog");
        
        gtk_init(NULL, NULL);
        
        window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
        gtk_window_set_title(GTK_WINDOW(window), "Configure plugin");
        gtk_window_set_default_size(GTK_WINDOW(window), 300, 400);
        g_signal_connect(G_OBJECT(window), "destroy", G_CALLBACK(gtk_main_quit), NULL);

        button = gtk_button_new_with_label("OK");
        g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(gtk_main_quit), NULL);
        gtk_container_add(GTK_CONTAINER(window), button);
        gtk_widget_show(button);
        
        gtk_widget_show(window);
        gtk_main();
}

static OPluginManagerPluginAuthorData authors[] = {
        {
                O_PLUGIN_MANAGER_PLUGIN_DATA_AUTHOR_MAGIC,
                "ikke",
                "eikke eikke com",
                "http://www.eikke.com"
        },
        {
                O_PLUGIN_MANAGER_PLUGIN_DATA_AUTHOR_MAGIC,
                NULL,
                NULL,
                NULL
        }
};

static OPluginManagerPluginData plugininfo = {
        O_PLUGIN_MANAGER_PLUGIN_DATA_MAGIC,
        TEST_LOADER_PLUGIN_MAGIC,
        "test-plugin1",
        "A simple test plugin, full-featured",
        "This is a sample test plugin, to test the OPluginManager functionality, giving sample usage of all possibilities",
        "0.1",
        "http://www.eikke.com",

        plugin_init,
        free_data,
        configure,

        authors
};

O_PLUGIN_MANAGER_PLUGIN_REGISTER(plugininfo);
