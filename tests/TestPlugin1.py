# OPluginManager
# Copyright (C) 2005, 2006 Nicolas Trangez <eikke@eikke.com>.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
# Boston, MA 02110-1301, USA.

from OPluginManagerPlugin import OPluginManagerPlugin, OPluginManagerPluginAuthor

class TestPlugin(OPluginManagerPlugin):
        def __init__(self):
                tmpauthor = OPluginManagerPluginAuthor("Ikke", "eikke eikke com", "http://www.eikke.com")
                OPluginManagerPlugin.__init__(self, "test-loader-plugin", "PythonTest1", "A little test plugin", tmpauthor, "Some longer description of this test plugin", "0.1", "http://www.eikke.com", self.Init, self.FreeData, self.Configure)

        def Configure(self):
                print "Configuring"

        def Init(self, init_data):
                print "Initializing with data \"" + init_data + "\""
                return "testplugindata"

        def FreeData(self, data):
                print "Freeing \"" + data + "\""

def OPluginManagerPluginInit():
        return TestPlugin()
