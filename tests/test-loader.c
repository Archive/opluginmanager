/* OPluginManager
 * Copyright (C) 2005, 2006 Nicolas Trangez <eikke@eikke.com>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include <glib.h>

#include "o-plugin-manager.h"
#include "o-plugin-manager-plugin.h"

#include "test-loader.h"

/* This is the number of valid modules for this test. Adjust it when more valid test cases are added */
#define NUM_VALID_MODULES 3

gint main(guint argc, gchar *argv[]) {
        guint cnt = 0;
        gchar *dir = NULL, *dir2 = NULL, *modulepath = NULL;
        const OPluginManagerPlugin *plugin = NULL;
        OPluginManager *manager = NULL;

        g_type_init();

        manager = O_PLUGIN_MANAGER(o_plugin_manager_new());
        o_plugin_manager_load_modules(manager, ".libs", TEST_LOADER_PLUGIN_MAGIC, "test-init-data", NULL);
        o_plugin_manager_load_modules(manager, ".", TEST_LOADER_PLUGIN_MAGIC, "test-init-data", NULL);
        g_print("\n\nFound %d modules\n", o_plugin_manager_get_num_modules(manager));

        /* Check whether invalid modules are not loaded, or valid modules are discarded */
        if(o_plugin_manager_get_num_modules(manager) != NUM_VALID_MODULES) {
                g_warning("Some valid modules are not loaded, or some invalid ones are loaded. Aborting");
                g_object_unref(manager);
                g_error("Abort");
        }

        /* Check whether we got a 0'st module, call its configure function */
        for(cnt = 0; cnt < o_plugin_manager_get_num_modules(manager); cnt++) {
                plugin = o_plugin_manager_get_module(manager, cnt);
                g_debug("Dumping module %d:\n", cnt);
                o_plugin_manager_plugin_dump(plugin);
                g_print("\n\n");
        }

        g_object_unref(manager);

        return 0;
}
