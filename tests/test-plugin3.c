/* OPluginManager
 * Copyright (C) 2005, 2006 Nicolas Trangez <eikke@eikke.com>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include <gtk/gtk.h>

#include "plugins/o-plugin-manager-plugin.h"
#include "plugins/o-plugin-manager-plugin-data.h"

#include "test-loader.h"

#define TEST_PLUGIN_MSG "[test-plugin2] "

static OPluginManagerPluginAuthorData authors[] = {
        {
                O_PLUGIN_MANAGER_PLUGIN_DATA_AUTHOR_MAGIC,
                "ikke",
                "eikke eikke com",
                "http://www.eikke.com"
        },
        {
                O_PLUGIN_MANAGER_PLUGIN_DATA_AUTHOR_MAGIC,
                NULL,
                NULL,
                NULL
        }
};

static OPluginManagerPluginData plugininfo = {
        O_PLUGIN_MANAGER_PLUGIN_DATA_MAGIC,
        "foobar",
        "test-plugin3",
        "A simple, small test plugin which should *not* be loaded",
        "plugin_magic checking plugin",
        "0.1",
        "http://www.eikke.com",

        NULL,
        NULL,
        NULL,

        authors
};

O_PLUGIN_MANAGER_PLUGIN_REGISTER(plugininfo);
